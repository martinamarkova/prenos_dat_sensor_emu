package weather.sensor.core;


import com.fasterxml.jackson.annotation.*;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "country",
        "description",
        "temp",
        "temp_min",
        "temp_max",
        "Cloudiness",
        "pressure",
        "humidity",
        "wind_speed"
})
/**
 * trieda, ktorá reprezentuje objekt údajov o počasí , ktoré sa odosielajú na backend,
 * obsahuje iba najdôležiješie údaje z dát, kt. sme prijali s openWeather
 * tento objekt sa bude posielať ako JSON smerom na server
 */
public class MyWeatherData {

    /**
     * default konštruktor
     */
    public MyWeatherData(){}

    /**
     * parametrický konštruktor, kt. vytvára objekt WeatherData z objektu openWeather
     * @param openWeather
     */
    public MyWeatherData(OpenWeather openWeather){
        this.id = openWeather.getId();
        this.name = openWeather.getName();
        this.country = openWeather.getSys().getCountry();
        this.description =openWeather.getWeather().get(0).getDescription();
        this.temp = openWeather.getMain().getTemp();
        this.tempMin = openWeather.getMain().getTempMin();
        this.tempMax = openWeather.getMain().getTempMax();
        this.cloudiness = openWeather.getClouds().getAll();
        this.pressure = openWeather.getMain().getPressure();
        this.humidity = openWeather.getMain().getHumidity();
        this.windSpeed = openWeather.getWind().getSpeed();
    }

    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("country")
    private String country;
    @JsonProperty("description")
    private String description;
    @JsonProperty("temp")
    private Double temp;
    @JsonProperty("temp_min")
    private Double tempMin;
    @JsonProperty("temp_max")
    private Double tempMax;
    @JsonProperty("Cloudiness")
    private Integer cloudiness;
    @JsonProperty("pressure")
    private Integer pressure;
    @JsonProperty("humidity")
    private Integer humidity;
    @JsonProperty("wind_speed")
    private Double windSpeed;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return id konkrétnej stanice, z ktorej pochádzajú dáta
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     * setter, ktorý slúži na nastavenie id stanice
     * @param id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return meno stanice
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * setter pre nastavenie mena stanice
     * @param name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return country
     */
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    /**
     * setter pre nastavenie krajiny
     * @param country
     */
    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return description stanice
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * setter pre nastavenie description stanice
     * @param description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return temperature
     */
    @JsonProperty("temp")
    public Double getTemp() {
        return temp;
    }

    /**
     * setter pre nastavenie temperature
     * @param temp
     */
    @JsonProperty("temp")
    public void setTemp(Double temp) {
        this.temp = temp;
    }

    /**
     *
     * @return temperature min
     */
    @JsonProperty("temp_min")
    public Double getTempMin() {
        return tempMin;
    }

    /**
     * setter pre nastavenie temperature min
     * @param tempMin
     */
    @JsonProperty("temp_min")
    public void setTempMin(Double tempMin) {
        this.tempMin = tempMin;
    }

    /**
     *
     * @return temperature max
     */
    @JsonProperty("temp_max")
    public Double getTempMax() {
        return tempMax;
    }

    /**
     * setter pre nastavenie temperature max
     * @param tempMax
     */
    @JsonProperty("temp_max")
    public void setTempMax(Double tempMax) {
        this.tempMax = tempMax;
    }

    /**
     *
     * @return cloudiness
     */
    @JsonProperty("Cloudiness")
    public Integer getCloudiness() {
        return cloudiness;
    }

    /**
     * setter pre nastavenie cloudiness
     * @param cloudiness
     */
    @JsonProperty("Cloudiness")
    public void setCloudiness(Integer cloudiness) {
        this.cloudiness = cloudiness;
    }

    /**
     *
     * @return pressure
     */
    @JsonProperty("pressure")
    public Integer getPressure() {
        return pressure;
    }

    /**
     * setter pre nastavenie pressure
     * @param pressure
     */
    @JsonProperty("pressure")
    public void setPressure(Integer pressure) {
        this.pressure = pressure;
    }

    /**
     *
     * @return humidity
     */
    @JsonProperty("humidity")
    public Integer getHumidity() {
        return humidity;
    }

    /**
     * setter pre nastavenie humidity
     * @param humidity
     */
    @JsonProperty("humidity")
    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    /**
     *
     * @return wind-speed
     */
    @JsonProperty("wind_speed")
    public Double getWindSpeed() {
        return windSpeed;
    }

    /**
     * setter pre nastavenie wind-speed
     * @param windSpeed
     */
    @JsonProperty("wind_speed")
    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     *
     * @return additional preperty
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * setter pre nastavenie additional property
     * @param name
     * @param value
     */
    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}