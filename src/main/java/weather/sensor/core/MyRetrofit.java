package weather.sensor.core;

        import retrofit2.Call;
        import retrofit2.http.POST;
        import retrofit2.http.QueryMap;

        import java.util.Map;

/**
 * Retrofit interface, pre funkcie ktoré je možné volať nad openWeather
 */
public interface MyRetrofit {
    @POST("weather")
    Call<OpenWeather> get_DataFromSourceServer(@QueryMap Map<String, String> param);
}
