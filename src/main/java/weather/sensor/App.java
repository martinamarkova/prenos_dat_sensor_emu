package weather.sensor;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import weather.sensor.API.MeteoStation;
import weather.sensor.core.*;
import weather.sensor.resources.MeteoStationResource;

//import com.howtodoinjava.rest.controller.EmployeeRESTController;

/**
 * hlavná trieda programu (obsahuje funkciu main)
 *
 */
public class App extends Application<Configuration>
{
    /**
     * retrofit klient pre OprenWeather
     */
    public static MyRetrofit weatherRetrofit;
    /**
     * retrofit klient pre Backend
     */
    public static BackendRetrofit backEndRetrofit;
    /**
     * objekt meteostanice reprezentujíci toto zariadenie
     */
    private static MeteoStation meteoStation;
    private static String configFile ;

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    /**
     * @return retrofit klient pre OprenWeather
     */
    public static MyRetrofit getMyRetrofit(){
        return weatherRetrofit;
    }

    /**
     * @return retrofit klient pre Backend
     */
    public static BackendRetrofit getBackEndRetrofit(){
        return backEndRetrofit;
    }

    /**
     * inicializácia
     * @param b konfigurácia
     */
    @Override
    public void initialize(Bootstrap<Configuration> b) {
    }

    /**
     * funkcia programu inicializácia prostreda, registrovanie resourcou a spustenie programu
     * @param c konfigurácia
     * @param e dropwizard prostredie
     * @throws Exception
     */
    @Override
    public void run(Configuration c, Environment e) throws Exception {
        LOGGER.info("Registering REST resources");

        final MeteoStationResource meteoStationResource= new MeteoStationResource(meteoStation);
        e.jersey().register(meteoStationResource);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        weatherRetrofit = retrofit.create(MyRetrofit.class);


        MeteoStationCredentials credential = MeteoStationCredentials.getMeteoStationCredentialsByConfigFile(configFile);
        backEndRetrofit = createAuthRetrofitService("http://localhost:8085/api/weatherData/",credential.getName(),credential.getPassword()).create(BackendRetrofit.class);
    }

    /**
     * vstupný bod programu
     * @param args args[0] = "serve", args[1] = menoKonfiguračnéhoSúboru.yml
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        configFile = args[1];
        meteoStation = new MeteoStation(configFile);
        new App().run(args);
    }

    /**
     * vytvorenie backend ŕetrofit klienta
     * @param baseUrl
     * @param user
     * @param pass
     * @return
     */
    private static Retrofit createAuthRetrofitService(String baseUrl, String user, String pass) {
        MeteoInterceptor interceptor = new MeteoInterceptor(user,pass);
        //hladať tu ak nebude fungovať
        //interceptor.setLevel(MeteoInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        return retrofit;
    }


}
