package weather.sensor.core;

/**
 * @author Marková, Kubaš
 * tento enum poskytuje základné informácie o zariadenia(meteostaniciach)
 * je možné lvytvoriť len takú meteostanicu, ktorá má záznam v tomto enume
 */
public enum Location {
    ZILINA (3056508L, "Zilina,sk", "1b22cff43febba9ca125bb8403056508",8011,8021),
    TRNAVA(3057124L, "Trnava,sk", "1b22cff43febba9ca125bb8403057124",8012,8022),
    TRENCIN(3057140L, "Trencin,sk", "1b22cff43febba9ca125bb8403057140",8013,8023),
    PRESOV (723819L, "Presov,sk", "1b22cff43febba9ca125bb8403723819",8014,8024),
    NITRA(3058531L, "Nitra,sk", "1b22cff43febba9ca125bb8403058531",8015,8025),
    KOSICE(724443L, "Kosice,sk", "1b22cff43febba9ca125bb8403724443",8016,8026),
    BRATISLAVA(3060972L, "Bratislava,sk", "1b22cff43febba9ca125bb8403060972",8017,8027),
    BANSKABYSTRICA(3061186L, "Banska Bystrica,sk", "1b22cff43febba9ca125bb8403061186",8018,8028),
    NONE(0L, "none,none", "00000000000000000000000000000000",0000,0000);

    private Long id;
    private String locationString;
    private String APIKEY;
    private Integer appConectPort;
    private Integer adminConectPort;

    /**
     * getter pre AppConectPort
     * @return appConectPort
     */
    public Integer getAppConectPort() {
        return appConectPort;
    }

    /**
     * getter pre AdminConectPort
     * @return adminConectPort
     */
    public Integer getAdminConectPort() {
        return adminConectPort;
    }

    /**
     * konštruktor, ktorý zabezpečuje inicializáciu objektu
     * @param id ídentifikátor meteostanice
     * @param locationString poloha
     * @param APIKEY špeciálny kľúč, ktorý zabezpečuje autorizáciu zariadenia
     * @param appConectPort port, na kt. sa môže zariadenie pripojiť
     * @param adminConectPort
     */
    Location(Long id, String locationString, String APIKEY, Integer appConectPort, Integer adminConectPort) {
        this.id = id;
        this.locationString = locationString;
        this.APIKEY = APIKEY;
        this.appConectPort = appConectPort;
        this.adminConectPort = adminConectPort;
    }

    /**
     * getter pre Id
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * getter pre Id, kt. vráti Id ako string
     * @return id ako string
     */
    public String getIdtoString() {
        return this.id.toString();
    }

    /**
     * getter pre Location, kt. vráti Location ako string
     * @return location ako string
     */
    public String getLocationToString() {
        return this.locationString;
    }

    /**
     * getter pre ApiKey
     * @return ApiKey
     */
    public String getAPIKEY() {
        return APIKEY;
    }

    /**
     * funkcia, kt. vráti Location na základe mena mesta
     * @param stationLocationName
     * @return
     */
    public static Location getLocationByCity(String stationLocationName) {
        switch(stationLocationName){
            case "Zilina": return ZILINA;
            case "Presov": return PRESOV;
            case "Bratislava": return BRATISLAVA;
            case "Trnava": return TRNAVA;
            case "Trencin": return TRENCIN;
            case "Kosice": return KOSICE;
            case "BanskaBystrica": return BANSKABYSTRICA;
            case "Nitra": return NITRA;
            
            default: return NONE;
        }
    }

    /**
     * funkcia, kt. vráti Location na základe Config Filu
     * @param stationLocationName
     * @return
     */
    public static Location getLocationByConfigFile(String stationLocationName) {
        switch(stationLocationName){
            case "ZilinaConfig.yml": return ZILINA;
            case "PresovConfig.yml": return PRESOV;
            case "BratislavaConfig.yml": return BRATISLAVA;
            case "TrnavaConfig.yml": return TRNAVA;
            case "TrencinConfig.yml": return TRENCIN;
            case "KosiceConfig.yml": return KOSICE;
            case "BanskaBystricaConfig.yml": return BANSKABYSTRICA;
            case "NitraConfig.yml": return NITRA;

            default: return NONE;
        }
    }
}
