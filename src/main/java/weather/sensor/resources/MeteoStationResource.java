package weather.sensor.resources;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import weather.sensor.API.MeteoStation;
import weather.sensor.App;
import weather.sensor.core.MyWeatherData;
import weather.sensor.core.OpenWeather;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Path("meteo")
@Api(value = "Meteo stanica")
@Produces(MediaType.APPLICATION_JSON)

/**
 * trieda, ktorá obsahuje funkcie, kt. je možné volať volaním CRUD príkazmi z backendu
 * na backend a tiež aj na openWeatherServer
 */
public class MeteoStationResource {
    final private static String MY_OPENWEATHER_APIID = "1b22cff43febba9ca125bb8404e4f4ea";
    private MeteoStation meteoStation;
    private MyTimer myTimer;

    /**
     * default konštruktor
     */
    public MeteoStationResource(){};

    /**
     * konšturktor, kt. inicializuje MeteoStationResource
     * spúšťa vlákno, ktoré nezávisle v pravidelných intervaloch odosiela aktuálne informácie o
     * počasí na backend
     * @param meteoStation
     */
    public MeteoStationResource(MeteoStation meteoStation){
        this.meteoStation = meteoStation;
        this.myTimer = new MyTimer(this.meteoStation);
        this.myTimer.start();
    };

    /**
     * funkcia, kt. získava informácie o počasí z openWeather
     * @return
     */
    @POST
    @Path("/getWeather")
    @ApiOperation(value = "Informácie o počasí")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public OpenWeather getDataFromOpenWeather() {
        Map<String, String> codeRequest = new HashMap<>();
        codeRequest.put("id", this.meteoStation.getId().toString());
        codeRequest.put("appid", MY_OPENWEATHER_APIID);
        try {               //App.getRequest.... vráti retrofitClienta
            retrofit2.Response<OpenWeather> weatherResp = App.getMyRetrofit().get_DataFromSourceServer(codeRequest).execute();
            OpenWeather openWeather;
            if (weatherResp.isSuccessful()) {
                openWeather = weatherResp.body();
                return openWeather;
            } else {
                openWeather = new OpenWeather();
                openWeather.setName(weatherResp.toString());
                return openWeather;
            }

        } catch (IOException e) {
            OpenWeather openWeather = new OpenWeather();
            openWeather.setName("Exception");
            return null;
        }
    }

    /**
     * funkcia, kt. získava informácie o počasí z openWeather a uloží ich do okliešteného objektu myOpenWeather
     * @return myWeatherData s aktuálnymi informáciami o počasí
     */
    @POST
    @Path("/getShortWeather")
    @ApiOperation(value = "Skrátené informácie o počasí")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MyWeatherData getShortDataFromOpenWeather() {
        Map<String, String> codeRequest = new HashMap<>();
        codeRequest.put("id", this.meteoStation.getId().toString());
        codeRequest.put("appid", MY_OPENWEATHER_APIID);
        try {               //App.getRequest.... vráti retrofitClienta
            retrofit2.Response<OpenWeather> weatherResp = App.getMyRetrofit().get_DataFromSourceServer(codeRequest).execute();
            OpenWeather openWeather;
            //MyWeatherData myWeatherData;
            if (weatherResp.isSuccessful()) {
                openWeather = weatherResp.body();
                return new MyWeatherData(openWeather);
            } else {
                openWeather = new OpenWeather();
                openWeather.setName(weatherResp.toString());
                return new MyWeatherData(openWeather);
            }

        } catch (IOException e) {
            OpenWeather openWeather = new OpenWeather();
            openWeather.setName("Exception");
            return null;
        }
    }

    /**
     * funkcia, kt. vráti aktuálnu meteostanicu
     * @return
     */
    @POST
    @Path("/getMeteoTest")
    @ApiOperation(value = "Vrátenie objektu tejto meteo stanice(pre testovacie účely)")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MeteoStation getMeteoTest() {
        return this.meteoStation;
    }

    /**
     * funkcia, kt. slúži na testovanie autentifikácie
     * @return
     */
    @POST
    @Path("/testAuth")
    @ApiOperation(value = "Testovanie autentifikácie")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MeteoStation getTestAuthentication() {

        try {               //App.getRequest.... vráti retrofitClienta
            retrofit2.Response<MeteoStation> meteoResp = App.getBackEndRetrofit().sendLoginRequest().execute();
            MeteoStation meteoStation;
            //MyWeatherData myWeatherData;
            if (meteoResp.isSuccessful()) {
                meteoStation = meteoResp.body();
                return meteoStation;
            } else {
                meteoStation = new MeteoStation();
                meteoStation.setName(meteoResp.toString());
                return meteoStation;
            }

        } catch (IOException e) {
            MeteoStation meteoStation = new MeteoStation();
            meteoStation.setName("Exception");
            return null;
        }

    }

    /**
     * funkcia, kt. nastaví periódu odosielanie dát na server
     * @param periodTimerSeconds čas periódy v sekundách
     * @return
     */
    @POST
    @Path("/setPeriod")
    @ApiOperation(value = "Nastavenie periódy odosielanie dát na server")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response setTimer(@QueryParam("Seconds") Long periodTimerSeconds) {
        this.myTimer.setPeriodTime(periodTimerSeconds*1000);
        return Response.status(Response.Status.OK)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    /**
     * privátna trieda vlákna,v kt. beží pravidelní odosielanie počasia na server
     * a súčastne prijímanie dát z OpenWeather
     */
    private class MyTimer extends Thread {
        private MeteoStation meteoStation;
        private long periodTime;

        /**
         * konštruktor, kt. nastaví defaultnú periódu časovača
         * @param meteoStation
         */
        public MyTimer(MeteoStation meteoStation) {
            this.meteoStation = meteoStation;
            this.periodTime = 900000; //defaul timme of period 15 minutes
        }


        /**
         * nastaví periódu časovača
         * @param periodTimeMs
         */
        public void setPeriodTime(long periodTimeMs) {
            this.periodTime = periodTimeMs;
        }

        /**
         * hlavná funckia vlíkna, v kt. sa najskôr prijmu dáta pomocou retrofit klienta z OpenWeather
         * následne sa pomocou druhého retrefit klienta pošlú na backend
         */
        @Override
        public void run(){
            MyWeatherData myWeatherData;
            try {
                sleep(5000);
            } catch (InterruptedException e) { }
            myWeatherData = null;

            while (true) {
                /*
                try {
                    sleep(this.periodTime);
                } catch (InterruptedException e) { }
                myWeatherData = null;
                */

                Map<String, String> codeRequest = new HashMap<>();
                codeRequest.put("id", this.meteoStation.getId().toString());
                codeRequest.put("appid", MY_OPENWEATHER_APIID);
                try {               //App.getRequest.... vráti retrofitClienta
                    retrofit2.Response<OpenWeather> weatherResp = App.getMyRetrofit().get_DataFromSourceServer(codeRequest).execute();
                    OpenWeather openWeather;
                    //MyWeatherData myWeatherData;
                    if (weatherResp.isSuccessful()) {
                        openWeather = weatherResp.body();
                        myWeatherData = new MyWeatherData(openWeather);
                    } else {
                        openWeather = new OpenWeather();
                        openWeather.setName(weatherResp.toString());
                        myWeatherData = new MyWeatherData(openWeather);
                    }


                } catch (IOException e) {
                    //chybovy stav
                }

                try {               //App.getRequest.... vráti retrofitClienta
                    retrofit2.Response<MeteoStation> meteoResp = App.getBackEndRetrofit().saveWeatherData(myWeatherData.getId(),
                                    myWeatherData.getName(),
                                    myWeatherData.getCountry(),
                                    myWeatherData.getDescription(),
                                    myWeatherData.getTemp(),
                                    myWeatherData.getTempMin(),
                                    myWeatherData.getTempMax(),
                                    myWeatherData.getCloudiness(),
                                    myWeatherData.getPressure(),
                                    myWeatherData.getHumidity(),
                                    myWeatherData.getWindSpeed())
                            .execute();
                    MeteoStation meteoStation;
                    //MyWeatherData myWeatherData;
                } catch (IOException e) {
                    //chybovy stav
                }

                try {
                    sleep(this.periodTime);
                } catch (InterruptedException e) { }
                myWeatherData = null;

            }
        }
    }
}
