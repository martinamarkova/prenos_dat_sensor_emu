package weather.sensor.core;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import weather.sensor.API.MeteoStation;

/**
 * BackendRetrofit interface, pre funkcie ktoré je možné volať na backend
 */
public interface BackendRetrofit {
    @POST("loginMeteo")
    Call<MeteoStation> sendLoginRequest();

    @GET("saveWeather")
    Call<MeteoStation> saveWeatherData(@Query("id") Long id,
                                       @Query("name") String name,
                                       @Query("country") String country,
                                       @Query("description") String description,
                                       @Query("temp") Double temp,
                                       @Query("temp_min") Double tempMin,
                                       @Query("temp_max") Double tempMax,
                                       @Query("Cloudiness") Integer cloudiness,
                                       @Query("pressure") Integer pressure,
                                       @Query("humidity") Integer humidity,
                                       @Query("wind_speed") Double windSpeed);
}
