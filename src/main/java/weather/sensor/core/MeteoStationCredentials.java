package weather.sensor.core;

/**
 * enum slúži na poskytovanie prihlasovacích údajov
 */
public enum MeteoStationCredentials {
    ZILINA ("Zilina", "hesloZilina"),
    TRNAVA("Trnava", "hesloTrnava"),
    TRENCIN("Trencin", "hesloTrencin"),
    PRESOV ("Presov", "hesloPresov"),
    NITRA("Nitra", "hesloNitra"),
    KOSICE("Kosice", "hesloKosice"),
    BRATISLAVA("Bratislava", "hesloBratislava"),
    BANSKABYSTRICA("BanskaBystrica", "hesloBanskaBystrica"),
    NONE("none", "none");

    private String name;
    private String password;

    /**
     * konštruktor pre vytvorenie prihlasovacích údajov
     * @param name prihlasovacie meno
     * @param password prihlasovacie heslo
     */
    MeteoStationCredentials(String name, String password) {
        this.name = name;
        this.password = password;
    }

    /**
     * @return prihlasovacie meno
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return prihlasovacie heslo
     */
    public String getPassword() {
        return password;
    }

    /**
     * funkcia, kt. vráti enum prihlasovacích údajov podľa konfiguračného súboru
     * @param configFileName
     * @return enum prihlasovacích údajov
     */
    public static MeteoStationCredentials getMeteoStationCredentialsByConfigFile(String configFileName) {
        switch(configFileName){
            case "ZilinaConfig.yml": return ZILINA;
            case "PresovConfig.yml": return PRESOV;
            case "BratislavaConfig.yml": return BRATISLAVA;
            case "TrnavaConfig.yml": return TRNAVA;
            case "TrencinConfig.yml": return TRENCIN;
            case "KosiceConfig.yml": return KOSICE;
            case "BanskaBystricaConfig.yml": return BANSKABYSTRICA;
            case "NitraConfig.yml": return NITRA;

            default: return NONE;
        }
    }
}
