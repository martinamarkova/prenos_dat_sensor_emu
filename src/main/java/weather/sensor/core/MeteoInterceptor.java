package weather.sensor.core;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * trieda, kt. slúži na interpretáciu prihlasovacích údajov do hlavičky http
 */
public class MeteoInterceptor  implements Interceptor {
    private String credentials;

    private static final Logger LOGGER = LoggerFactory.getLogger(MeteoInterceptor.class);

    /**
     * default konštruktor, kt. vytvorí MeteoInterceptor
     */
    public MeteoInterceptor() {
        this.credentials = Credentials.basic(null, null);
    }

    /**
     * parametrický konštruktor, kt. vytvorí MeteoInterceptor
     */
    public MeteoInterceptor(String user, String password) {
        this.credentials = Credentials.basic(user, password);
    }

    /**
     * funkcia, kt. vkladá prihlasovacie údaje do http rámca
     * @param chain
     * @return
     * @throws IOException
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request authenticatedRequest = request.newBuilder()
                .header("Authorization", credentials).build();
        return chain.proceed(authenticatedRequest);
    }

}
