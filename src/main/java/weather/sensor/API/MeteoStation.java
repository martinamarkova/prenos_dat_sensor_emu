package weather.sensor.API;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableSet;
import weather.sensor.core.Location;
import weather.sensor.core.MeteoStationCredentials;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * objekt MeteoStanice, kt. obsahuje všetky dôležité infomácie o stanici(zariadení)
 */
public class MeteoStation  implements Principal {

    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String password;
    @JsonProperty
    private String location;
    @JsonProperty
    private Set<String> apiKey;
    @JsonProperty
    private String registered;
    @JsonProperty
    private Integer port;

    /**
     * default konštruktor
     */
    public MeteoStation(){}

    /**
     * parametrický konštruktor, kt. vytvorí objekt MeteoStanice podľ konfiguračného súboru
     * @param configFileName
     */
    public MeteoStation(String configFileName) {
        Location meteostationLocation = Location.getLocationByConfigFile(configFileName);
        MeteoStationCredentials credentials = MeteoStationCredentials.getMeteoStationCredentialsByConfigFile(configFileName);

        this.name = credentials.getName();
        this.password = credentials.getPassword();

        this.location = meteostationLocation.getLocationToString();
        this.id = meteostationLocation.getId();
        this.apiKey = ImmutableSet.of(meteostationLocation.getAPIKEY());
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        this.registered = formatter.format(date);
        this.port = meteostationLocation.getAppConectPort();
    }

    /**
     * parametrický konštruktor, ktorý inicializuje objekt MeteoStanice pomocou parametrov
     * @param name
     * @param password
     * @param location
     * @param id
     * @param apiKey
     * @param port
     */
    public MeteoStation(String name, String password, String location, Long id, Set<String> apiKey, Integer port){
        this.name = name;
        this.password = password;
        this.location = location;
        this.id = id;
        this.apiKey = apiKey;
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        this.registered = formatter.format(date);
        this.port = port;
    }

    /**
     *
     * @return meno stanice
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * setter pre nastavenie mena stanice
     * @param name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return password
     */
    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    /**
     * setter pre nastavenie passwordu
     * @param password
     */
    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return location
     */
    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    /**
     * setter pre nastavenie location
     * @param location
     */
    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return id stanice
     */
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    /**
     * setter pre nastavenie id stanice
     * @param id
     */
    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return apiKey
     */
    @JsonProperty("api_key")
    public Set<String> getApiKey() {
        return apiKey;
    }

    /**
     * setter pre nastavenie api kľúča
     * @param apiKey
     */
    public void setApiKey(Set<String> apiKey) {
        this.apiKey = apiKey;
    }

    /**
     *
     * @return registrovanie konkrétnej stanice
     */
    @JsonProperty("registered")
    public String getRegistered() {
        return registered;
    }

    /**
     * setter pre nastavenie registrácie stanice
     * @param registered
     */
    @JsonProperty("registered")
    public void setRegistered(String registered) {
        this.registered = registered;
    }

    /**
     *
     * @return port
     */
    @JsonProperty("port")
    public Integer getPort() {
        return port;
    }

    /**
     * setter pre nastavenie portu
     * @param port
     */
    @JsonProperty("port")
    public void setPort(Integer port) {
        this.port = port;
    }
}
